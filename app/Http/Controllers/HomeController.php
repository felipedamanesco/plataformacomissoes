<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use DB;
use PDO;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     **/
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function getTeamName($teamId)
    {

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $teamName = DB::table('team')
            ->select('name')
            ->where('id',$teamId)
            ->get();
        DB::setFetchMode(PDO::FETCH_CLASS);

        return $teamName[0]['name'];

    }

    public function construct_array()
    {
        $arr = array(
            '1' => [['Validar Avaliações', 'validar'], ['Avaliar Team Leaders', 'avaliar'], ['Ver avaliações', 'avaliacoes']],
            '2' => [['Validar Avaliações', 'validar'], ['Avaliar Team Leaders', 'avaliar'], ['Ver avaliações', 'avaliacoes']],
            '3' => [['Avaliar Colaboradores', 'avaliar'], ['Ver avaliações', 'avaliacoes']],
            '4' => [['Ver avaliações', 'avaliacoes']]);
        return $arr;
    }

    public function index()
    {
        //primeiro parametro de cada array como nome, segundo como link.
        $sections_buttons = $this->construct_array();
        return view('home', ['sections_buttons' => $sections_buttons]);
    }

    /**
     * @param $request para saber o nível de utilizador
     * @return array de titulos e links
     */
    public function switcher($request, $page)
    {
        if ($request->user()) {
            $user_level = $request->user()->level_id;
            $user_team = $request->user()->team_id;
            switch ($user_level) {
                case 1:
                case 2:
                    //primeiro parametro do array como nome, segundo como link.
                    $arr_buttons = array();
                    DB::setFetchMode(PDO::FETCH_ASSOC);
                    $usersFromMyTeam = DB::table('team')->select('id', 'name')->get();
                    DB::setFetchMode(PDO::FETCH_CLASS);

                    switch($page){
                        case 'avaliar':
                            foreach ($usersFromMyTeam as $number => $array) {
                                //primeiro parametro do array como nome, segundo como link.
                                $link = "$page/teamleader/{" . $array['id'] . "}";
                                $arr_buttons[] = array($array['name'], $link);
                            }
                        break;
                        case 'avaliacoes':
                            foreach ($usersFromMyTeam as $number => $array) {
                                //primeiro parametro do array como nome, segundo como link.
                                $link = "$page/{" . $array['id'] . "}";
                                $arr_buttons[] = array($array['name'], $link);
                            }
                        break;
                        case 'validar':
                            foreach ($usersFromMyTeam as $number => $array) {
                                //primeiro parametro do array como nome, segundo como link.
                                $link = "$page/campanhas/{" . $array['id'] . "}";
                                $arr_buttons[] = array($array['name'], $link);
                            }
                        break;
                    }

                break;
                case 3:
                    $arr_buttons = array();
                    DB::setFetchMode(PDO::FETCH_ASSOC);
                        $usersFromMyTeam = DB::table('users')->select('id', 'name')->where('team_id', $user_team)->where('level_id', '4')->get();
                    DB::setFetchMode(PDO::FETCH_CLASS);

                    foreach ($usersFromMyTeam as $number => $array) {
                        //primeiro parametro do array como nome, segundo como link.
                        $link = "$page/colaboradores/{" . $array['id'] . "}";
                        $arr_buttons[] = array($array['name'], $link);
                    }

                break;
            }

            return $arr_buttons;
        }
    }

    public function avaliar(Request $request)
    {
        $page = 'avaliar';
        $arr_buttons = $this->switcher($request, $page);
        return view('avaliar', ['arr_buttons' => $arr_buttons]);
    }

    public function avaliacoes(Request $request)
    {
        $check_level = $request->user()->level_id;
        switch($check_level){
            case 1:
            case 2:

            break;
            case 3:
                DB::setFetchMode(PDO::FETCH_ASSOC);
                $shares = DB::table('evaluation_summary')
                    ->join('evaluation', 'evaluation.id', '=', 'evaluation_summary.evaluation_id')
                    ->join('campaign', 'campaign.id', '=', 'evaluation.campaign_id')
                    ->join('users', 'users.id', '=', 'user_evaluator_id')
                    ->where('team_id',$request->user()->team_id)
                    ->get();
                DB::setFetchMode(PDO::FETCH_CLASS);
                $tabelaFinal = array();
                $tabela = array();
                foreach($shares as $number => $array){
                    $tabela['data'] = $array['date'];
                    $tabela['colaborador'] = $array['name'];
                    $tabela['unidade de negocio'] = $this->getTeamName($array['team_id']);
                    $tabela['team leader'] = $request->user()->name;
                    $tabelaFinal[] = $tabela;
                }

            break;
        }

        return view('avaliacoes.colaboradores', ['tabelaFinal'=>$tabelaFinal]);
    }

    public function validar(Request $request)
    {
        $page = 'validar';
        if($request->user()->id > 3) {
            $arr_buttons = $this->switcher($request, $page);
            return view('validar', ['arr_buttons' => $arr_buttons]);
        }else{
            $sections_buttons = $this->construct_array();
            return view('home', ['sections_buttons' => $sections_buttons]);
        }
    }

    public function avaliaruser(Request $request, $user_id)
    {
        $user_id = str_replace('{', '', $user_id);
        $user_id = str_replace('}', '', $user_id);
        DB::setFetchMode(PDO::FETCH_ASSOC);
            $data_list = DB::table('campaign')->select('name')->get();
            $user_name = DB::table('users')->select('name')->where('id', $user_id)->get();
            $user_team = DB::table('users')->select('team_id')->where('id', $user_id)->get();
            $evaluation_scores  = DB::table('evaluation_scores')->select('*')->get();
            $transaction_scores = DB::table('transaction_scores')->select('*')->get();
        DB::setFetchMode(PDO::FETCH_CLASS);
        $user_teamId = $user_team[0]['team_id'];
        $pass = ['name'=>$user_name, 'transactions'=>$transaction_scores, 'evaluation'=>$evaluation_scores];
        return view('avaliar.colaboradores', ['pass'=>$pass, 'user_id'=>$user_id, 'user_teamId'=>$user_teamId, 'data_list'=>$data_list]);
    }

    public function avaliarteamleader(Request $request, $user_id)
    {
        $user_id = str_replace('{', '', $user_id);
        $user_id = str_replace('}', '', $user_id);
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $user_name = DB::table('users')->select('name')->where('team_id', $user_id)->where('level_id','3')->get();
        $evaluation_scores  = DB::table('evaluation_scores')->select('*')->get();
        $transaction_scores = DB::table('transaction_scores')->select('*')->get();
        DB::setFetchMode(PDO::FETCH_CLASS);
        $pass = ['name'=>$user_name, 'transactions'=>$transaction_scores, 'evaluation'=>$evaluation_scores];
        return view('avaliar.teamleader', ['pass'=>$pass, 'user_teamId'=>$user_id]);
    }

    public function validarcampanha($id)
    {
        var_dump($id);
        return view('validar.campanhas');
    }



}

















/*public function next()
     $finalTable = array();
        $evaluation_summary = array();
        $campaigns = array();

     DB::setFetchMode(PDO::FETCH_ASSOC);
            $evaluation_summary = DB::table('evaluation_summary')->select('*')->where('user_evaluator_id', $user_id)->get();
            foreach($evaluation_summary as $summary=>$array){
                $evaluation_table= DB::table('evaluation')->select('*')->where('id', $array['evaluation_id'])->get();
                foreach($evaluation_table as $num=>$arr){
                    $campaigns = DB::table('campaign')->select('*')->where('id', $arr['campaign_id'])->get();
                }
            }

        DB::setFetchMode(PDO::FETCH_CLASS);

        echo "<pre>";
        print_r($evaluation_table);
        echo "</pre>";
        echo "<pre>";
        print_r($campaigns);
        echo "</pre>";

        echo "<pre>";
        print_r($evaluation_summary);
        echo "</pre>";
    */

