<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
      DB::table('users')->insert([
        'level_id'=>'3',
        'team_id'=>'1',
        'name'=>'Mauro Gama',
        'email'=>'mauro.gama@actualsales.com',
        'password'=>Hash::make('maurogama2016!'),

    ]);

        DB::table('users')->insert([
            'level_id'=>'4',
            'team_id'=>'1',
            'name'=>'João Serra',
            'email'=>'joao.serra@actualsales.com',
            'password'=>Hash::make('joaoserra!'),

        ]);

        DB::table('users')->insert([
            'level_id'=>'2',
            'team_id'=>'1',
            'name'=>'Bull',
            'email'=>'bull@actualsales.com',
            'password'=>Hash::make('bull_'),

        ]);


    }


}
