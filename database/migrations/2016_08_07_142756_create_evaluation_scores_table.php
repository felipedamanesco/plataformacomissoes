<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //esta tabela cria as evaluations scores.
    public function up()
    {
        Schema::create('evaluation_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('score');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluation_scores');
    }
}
