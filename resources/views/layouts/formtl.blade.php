
    <script type="text/javascript" src="{!! asset('script/newElements.js') !!}"></script>
    <div class="container">
        <datalist id="campaign">
            @foreach($data_list as $number=>$name)
                <option value="{{$name['name']}}">
            @endforeach
        </datalist>

        <div class="panel panel-default">

            <div class="panel-heading"><h2>{{$pass['name'][0]['name']}}</h2></div>
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{url('/form-submit/teamleader')}}">
                    {{ csrf_field() }}

                    <div id="add_inputs">
                        <div class="col-md-2">
                            <input type="text" list="campaign" class="form-control" placeholder="Nome da Campanha" name="name" value="{{ old('name') }}">
                        </div>

                        <div class="col-md-2">
                            <input type="text" class="form-control" placeholder="Margem Bruta €" name="margem" value="{{ old('margem') }}">
                        </div>

                        <div class="col-md-2">
                            <select id="evaluation" class="evaluation" name="evaluation">
                                <option value="#" selected="selected">Avaliação</option>
                                @foreach($pass['evaluation'] as $num => $arr)
                                    <option value="{{$arr['id']}}">{{$arr['score']}}%</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Adiciona aqui uma nota" name="nota">
                        </div>
                    </div>

                    <div class="form-group">
                        <div  style="float:right!important; margin-top:20px; margin-right:20px;">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-send"></i> Enviar
                            </button>
                        </div>
                    </div>
                </form>
                <div  style="float:right!important; margin-top:20px;">
                    <button class="btn " id="add" value="1" onclick="addMore(value)">
                        <i class="fa fa-btn"></i> Adicionar Campanha
                    </button>
                </div>
            </div>
        </div>

    </div>
