<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDO;
use Carbon\Carbon;
use App\Http\Requests;
use DB;
class DbController extends Controller
{

    public function construct_array()
    {
        $arr = array(
            '1' => [['Validar Avaliações', 'validar'], ['Avaliar Team Leaders', 'avaliar'], ['Ver avaliações', 'avaliacoes']],
            '2' => [['Validar Avaliações', 'validar'], ['Avaliar Team Leaders', 'avaliar'], ['Ver avaliações', 'avaliacoes']],
            '3' => [['Avaliar Colaboradores', 'avaliar'], ['Ver avaliações', 'avaliacoes']],
            '4' => [['Ver avaliações', 'avaliacoes']]);
        return $arr;
    }
    //
    public function colab(Request $request)
    {
        var_dump($request->all());
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $campaign = DB::table('campaign')->select('id')->where('name', $request['name'])->count();
        if ($campaign == 0) {
            DB::table('campaign')->insert([
                'id' => '',
                'name' => $request['name'],
            ]);
            $campaign = DB::table('campaign')->select('id')->where('name', $request['name'])->get();
            $campaign = $campaign[0]['id'];
        }else{
            $campaign = DB::table('campaign')->select('id')->where('name', $request['name'])->get();
            $campaign = $campaign[0]['id'];
        }

        $evaluation_id = DB::table('evaluation')->select('id')->where('transaction_scores_id', $request['transition'])
            ->where('evaluation_scores_id', $request['evaluation'])->where('campaign_id', $campaign)->count();
        if ($evaluation_id == 0) {
            DB::table('evaluation')->insert([
                'id' => '',
                'transaction_scores_id' => $request['transition'],
                'evaluation_scores_id' => $request['evaluation'],
                'campaign_id' => $campaign,
                'notes' => $request['nota']

            ]);
            $evaluation_id = DB::table('evaluation')->select('id')->where('transaction_scores_id', $request['transition'])
                ->where('evaluation_scores_id', $request['evaluation'])->where('campaign_id', $campaign)->get();
            $evaluation_id = $evaluation_id[0]['id'];
        }else{
            $evaluation_id = DB::table('evaluation')->select('id')->where('transaction_scores_id', $request['transition'])
                ->where('evaluation_scores_id', $request['evaluation'])->where('campaign_id', $campaign)->get();
            $evaluation_id = $evaluation_id[0]['id'];
        }
        $data = Carbon::now();
        $data->toDateTimeString();
        $data = (array)$data;
        $data = date('Y-m-d');
        $last = DB::table('evaluation_summary')->select('*')->where('user_evaluator_id', $request['evaluator'])->where('evaluation_id', $evaluation_id)->where('date', $data)->count();
        if ($last == 0) {
            DB::table('evaluation_summary')->insert([
                'id' => '',
                'user_evaluator_id' => $request['evaluator'],
                'evaluation_id' => $request['evaluation'],
                'date' => $data,
                'note' => $request['big-notes']

            ]);
        }
        $sections_buttons = $this->construct_array();
        return $request['evaluator'];
    }

    public function tl(Request $request)
    {
        return $request->all();
    }

}
