
@extends('layouts.app')

@section('content')

    <center>
        @foreach($arr_buttons as $number => $arr)

            <a href={{url('/'.$arr[1])}}><div class="btn btn-default">{{$arr[0]}}</div></a>&nbsp;&nbsp;&nbsp;

        @endforeach
    </center>

@endsection