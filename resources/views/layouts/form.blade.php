@if(Auth::user()->id == $user_id)
    @elseif(Auth::user()->team_id == $user_teamId)
    <script type="text/javascript" src="{!! asset('script/newElements.js') !!}"></script>
        <div class="container">

            <datalist id="campaign">
                @foreach($data_list as $number=>$name)
                    <option value="{{$name['name']}}">
                @endforeach
            </datalist>

            <div class="panel panel-default">

                <div class="panel-heading"><h2>{{$pass['name'][0]['name']}}</h2></div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{url('/form-submit')}}">
                        {{ csrf_field() }}

                        <input type="hidden" value="{{$user_id}}" name="evaluator">
                        <div id="add_inputs">
                            <div class="col-md-2">
                                <input type="text" list="campaign" class="form-control" placeholder="Nome da Campanha" name="name" required>
                            </div>

                            <div class="col-md-2">
                                <input type="text" class="form-control" placeholder="Margem Bruta €" name="margem" required>
                            </div>

                            <div class="col-md-2">
                                <select id="evaluation" class="evaluation" name="evaluation" required>
                                    <option value="#" selected="selected">Avaliação</option>
                                    @foreach($pass['evaluation'] as $num => $arr)
                                        <option value="{{$arr['id']}}">{{$arr['score']}}%</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2">
                                <select id="transaction" class="transition" name="transition" required>
                                    <option value="#" selected="selected">Transição</option>
                                    @foreach($pass['transactions'] as $num => $arr)
                                            <option value="{{$arr['id']}}">{{$arr['score']}}%</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Adiciona aqui uma nota" name="nota">
                            </div>
                        </div>

                        <textarea name="big-notes"></textarea>

                <div class="form-group">
                    <div  style="float:right!important; margin-top:20px; margin-right:20px;">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-send"></i> Enviar
                        </button>
                    </div>
                </div>
                    </form>
                    <div  style="float:right!important; margin-top:20px;">
                        <button class="btn " id="add" value="1" onclick="addMore(value)">
                            <i class="fa fa-btn"></i> Adicionar Campanha
                        </button>
                    </div>
                </div>
            </div>

    </div>
@endif