<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {

        return view('auth.login');
});


Route::auth();


Route::get('home/', 'HomeController@index');
Route::get('validar/','HomeController@validar');
Route::get('avaliar/','HomeController@avaliar');
Route::get('avaliacoes/','HomeController@avaliacoes');
Route::get('avaliar/colaboradores/{user_id}', ['uses'=>'HomeController@avaliaruser']);
Route::get('avaliar/teamleader/', 'HomeController@avaliacoescolab');
Route::get('validar/campanhas/{id}', ['uses'=>'HomeController@validarcampanha']);


Route::post('form-submit', ['uses'=>'DbController@colab']);
Route::post('form-submit/teamleader', ['uses'=>'DbController@tl']);