
@extends('layouts.app')


@section('content')


   @if(Auth::user()->level_id == 1)
       <center><h1>Olá Admin</h1></br>
      @foreach($sections_buttons[1] as $num => $ref)
               <a href={{url("/".$ref[1])}}><div class="btn btn-default">{{$ref[0]}}</div></a>&nbsp;&nbsp;&nbsp;
      @endforeach
       </center>
    @endif

   @if(Auth::user()->level_id == 2)
       <center><h1>Olá BULL</h1></br>
           @foreach($sections_buttons[2] as $num => $ref)
               <a href={{url("/".$ref[1])}}><div class="btn btn-default">{{$ref[0]}}</div></a>&nbsp;&nbsp;&nbsp;
           @endforeach
       </center>
   @endif

   @if(Auth::user()->level_id == 3)
       <center><h1>Olá Team Leader {{Auth::user()->name}}</h1></br>
           @foreach($sections_buttons[3] as $num => $ref)
               <a href={{url("/".$ref[1])}}><div class="btn btn-default">{{$ref[0]}}</div></a>&nbsp;&nbsp;&nbsp;&nbsp;
           @endforeach
       </center>
   @endif

   @if(Auth::user()->level_id == 4)
       <center><h1>Olá {{Auth::user()->name}}</h1></br>
           @foreach($sections_buttons[4] as $num => $ref)
               <a href={{url("/".$ref[1])}}><div class="btn btn-default">{{$ref[0]}}</div></a>&nbsp;&nbsp;&nbsp;&nbsp;
           @endforeach
       </center>
   @endif

@endsection
